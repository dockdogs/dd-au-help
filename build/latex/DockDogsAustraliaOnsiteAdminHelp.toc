\select@language {english}
\contentsline {chapter}{\numberline {1}Getting Started}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}Providing Basic Assistance To Handlers}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Assisting A Handler With Locating Their Username}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Assisting A Handler With Resetting Their Password}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Assisting A Handler With Editing Their Profile}{5}{section.2.3}
\contentsline {section}{\numberline {2.4}Assisting A Handler With Changing Their Password}{6}{section.2.4}
\contentsline {chapter}{\numberline {3}Assisting A Handler With Teams}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Assisting A Handler With Adding a Team (Dog)}{7}{section.3.1}
\contentsline {chapter}{\numberline {4}Registering For Events}{9}{chapter.4}
\contentsline {section}{\numberline {4.1}Handler Event Registration Walkthrough Video}{9}{section.4.1}
\contentsline {section}{\numberline {4.2}Step By Step Event Registration}{9}{section.4.2}
