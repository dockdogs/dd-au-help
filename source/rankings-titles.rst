Rankings & Titles
=======================

DockDogs Titles
----------------------

* `DockDogs Titles`_ reports can be found on the DockDogs Website which list all teams currently Titled in either Club or National Titles.
* Teams listed in the `DockDogs Titles`_ reports must be **ACTIVE DockDogs Worldwide Members** and have achieved the required number of legs in a specific *Division* of a specific *Discipline*. For more information regarding the required number of Legs please see the `Rules and Policies`_.
* Handlers that have team(s) that have earned a DockDogs Title can download their Title Certificate from their `MyDockDogs Dashboard`_ by logging in with their username and password. 

.. tip:: Titles are cumulative and do not reset each season. *Example: If a team has 4 Elite Big Air National event legs during 2016 and in 2017 earns the 5th leg required for this Title they will now be Titled, even though it is now the 2017 season.*

DockDogs Rankings
---------------------

.. tip:: `DockDogs Rankings`_ unlike `DockDogs Titles`_ are reset each Season. Specific dates regarding the end of a DockDogs Season can be found in the `Rules and Policies`_.

* `DockDogs Rankings`_ can be accessed via the DockDogs website in web view or PDF download using the various controls for filtering which Ranking type the Handler is interested in.
* `DockDogs Rankings`_ can also be accessed by Handlers by logging into their `MyDockDogs Dashboard`_ account with their Username and Password and clicking on **RANKINGS** from the upper right hand corner of the screen. 

	* Just like with the Rankings on the `DockDogs Rankings`_ page of the website the Handler must select from the dropdown boxes the rankings type they wish to obtain. 
	* Rankings are only available for the **CURRENT SEASON**. 







.. target-notes::

.. _`DockDogs Titles`: https://dockdogs.com/rankings-results/dockdogs-titles/
.. _`Rules and Policies`: https://dockdogs.com/events/rules-policies/
.. _`MyDockDogs Dashboard`: http://dockdogsevents.com
.. _`DockDogs Rankings`: https://dockdogs.com/rankings-results/rankings/

