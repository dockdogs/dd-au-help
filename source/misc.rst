Miscellaneous 
=================

This section is for content that does not fit elsewhere in the guide.

Communicating With Handlers Over Email
-----------------------------------------------

.. tip:: Your email address trudi.vandewint@dockdogs.com should not be used for communicating with Handlers regarding how to register for events. Instead, use the email address **auevents@dockdogs.com** to send and receive email when corresponding with Handlers. The guide below will walk you through how to do this on both the computer using the Gmail web interface as well as Gmail.app on your iPhone or Android device.

Checking the auevents@dockdogs.com email address
**************************************************

Emails sent to auevents@dockdogs.com will appear in your normal trudi.vandewint@dockdogs.com email inbox, when you reply to an email sent to this address it will by default reply from auevents@dockdogs.com.

Gmail on Computer
**********************

* Click **COMPOSE** to begin a new email message.
* Click the **FROM** line and select **auevents@dockdogs.com**
* Enter your **TO** to line to the address you wish to email.
* Type your email.
* Send your email.

.. image:: images/gmail-alias.gif

* To see full size version of image right click and say "Open image in new tab"*

Gmail Mobile App for iPhone & Android
****************************************

.. tip:: Just like on the Gmail on a Computer steps above when just simply click on the **FROM** line to select which email to send your email from. Follow the same procedures accordingly when on mobile. 

.. raw:: html

	<iframe src="https://drive.google.com/a/dockdogs.com/file/d/0B7ZexPZHl4r8b2phUloyWUQtNkE/preview" width="640" height="480"></iframe>