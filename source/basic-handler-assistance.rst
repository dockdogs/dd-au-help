Providing Basic Assistance To Handlers
============================================

Assisting A Handler With Locating Their Username
-------------------------------------------------

* Once you've logged in via the admin login you're able to search for Handler accounts.
* Click **MANAGE** --> **HANDLERS & TEAMS** from the menu along the top of the page.
* Enter the Handler's **first** & **last name** into the search boxes on the left hand side of the page.
* Click **SEARCH**

.. tip:: Handlers can also locate their **username and/or password** by visiting `http://dockdogsevents.com <http://dockdogsevents.com>`_ and clicking on **FORGOT USERNAME** or **FORGOT PASSWORD**. *Their details will be sent via email via an automated system, if they do not receive the email please ask them to check their spam/junk filter. If they still cannot locate the email confirm that the email address on file is correct and that's the email they are checking.*

Assisting A Handler With Resetting Their Password
---------------------------------------------------

* Handlers can reset their password via the self-service automated method at `https://dockdogsevents.com <https://dockdogsevents.com>`_ as long as they already know their **USERNAME** and **EMAIL ADDRESS** in our database.
* From your admin account you can **MANAGE** --> **HANDLERS & TEAMS** from the menu along the top of the page and locate a Handler via the search filters to assist a Handler with the password reset process.

.. tip:: Handler passwords are encrypted and subsequently can only be reset, viewing the current password isn't possible. This is to protect the security of the Handler's account.

Assisting A Handler With Editing Their Profile
------------------------------------------------

* Handlers can click on their name in the upper right hand corner and click on **EDIT PROFILE**.
* This will provide them with the ability to edit their phone numbers, emails, postal mailing address, etc.

Assisting A Handler With Changing Their Password
----------------------------------------------------

* Handlers can click on their name in the upper right hand corner and click on **EDIT PROFILE**.
* Handlers can click on **CHANGE PASSWORD** once on the **EDIT PROFILE** screen.
* There will be 3 boxes displayed to the handler.

    * The top box the Handler needs to enter their **CURRENT PASSWORD**.
    * The bottom two boxes the Handler will enter their **NEW PASSWORD**.
