Assisting A Handler With Teams
===================================

Assisting A Handler With Adding a Team (Dog)
-------------------------------------------------

* Often times a Handler may get stuck when attempting to register due to not having added a team (dog) to their account.
* You can confirm that a Handler does/does not have teams on their account by clicking on the **ACTION** button on the **MANAGE** --> **HANDLERS & TEAMS** screen of a given handler and selecting **EDIT TEAM**.

.. image:: images/handler-has-teams.gif

* If a Handler doesn't have any teams they must add a team (dog) to their account prior to being able to register for any events.
* A Handler can add a Team (dog) to their account by logging in at `https://dockdogsevents.com <https://dockdogsevents.com>`_ with their username and password, selecting **TEAMS** from the upper right hand corner, clicking on **ADD NEW TEAM** to enter their dogs details, then **SAVE** the record.
