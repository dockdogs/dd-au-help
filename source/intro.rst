Getting Started
======================

These steps will guide you through signing in as an ONSITE ADMIN to begin assisting Handlers with their registration queries. Please continue to read on for assisting Handlers once signed in to the Manager interface.

* Open your web browser *(Example: Google Chrome)*.
* Navigate to `http://manager.dockdogsevents.com <http://manager.dockdogsevents.com>`_.
* Enter your **ADMIN** username and password in the corresponding boxes.
* Click **LOGIN**.

.. tip:: You must use a computer (laptop/desktop) to interface with this website. Using a mobile phone or tablet is not supported at this time.
