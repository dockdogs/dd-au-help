
Welcome to DockDogs Australia Onsite Admin Help's documentation!
================================================================

.. tip:: CONFIDENTIAL -- THIS IS FOR USE BY DOCKDOGS AUSTRALIA ADMIN ONLY, NOT FOR COMPETITOR USE OR FOR DISTRIBUTION IN ANY MANNER.

.. toctree::
   :maxdepth: 2
   :caption: DockDogs Australia Admin Details

   intro
   basic-handler-assistance
   teams 
   event-registration
   rankings-titles

.. toctree::
   :maxdepth: 2
   :caption: Misc.

   misc