Registering For Events
===========================

This will provide instructions regarding registering for events.

.. tip:: As an **ONSITE ADMIN** you cannot process an event registration for a handler on their behalf. However, you can assist them by walking through the registration process.

Handler Event Registration Walkthrough Video
----------------------------------------------

.. image:: images/event-registration-handler.gif

Step By Step Event Registration
-------------------------------------------------

* Handler signs in in at `http://dockdogsevents.com`_ using their Username & Password.
* Handler clicks on **EVENTS** --> **UPCOMING EVENTS**
* Handler locates the event they wish to register for from the list of all open events.
* Handler clicks on **EVENT NAME** to view the event schedule and continue registering.
* Handler clicks on **REGISTER FOR THIS EVENT** button.

.. image:: images/register-button.png

* Handler chooses from list of teams *(dogs)*
* If no teams are shown here please see the `teams <https://dockdogs-australia-admin.aerobatic.io/teams>`_ page as they do not yet have any teams (dogs) added to their account. They need to follow these steps prior to proceeding.

.. image:: images/teams-list.png

.. tip:: Teams with a **BLUE** arrow are either DockDogs Worldwide Members or have received not exhausted *(used)* their two (2) transactions per season of pre-registration prior to becoming a Worldwide Member. Teams with a **BLACK** arrow have used their two (2) transactions this season and are **NOT** DockDogs Worldwide Members, subsequently they must either **PURCHASE A MEMBERSHIP** or **REGISTER ONSITE ON A FIRST COME, FIRST SERVE BASIS**.

* Handler clicks on the **BLUE ARROW** of the corresponding row of the team they wish to register for.
* Handler is taken to **WAVE SELECTION** where they can select which Wave(s) they wish to participate in. Upon selecting all the Waves they wish to participate in with this team they can click **NEXT**.
* On the **CONFIRMATION** tab Handler can select either **ADD ANOTHER TEAM** or **MAKE PAYMENT**.

    * If they select **ADD ANOTHER TEAM** they can select another team *(dog)* by clicking on the corresponding **BLUE ARROW** and select the Wave(s) for this 2nd team.
    * If they select **MAKE PAYMENT** they'll be taken to a page where they can enter their Credit Card number and billing details.

* On the Credit Card payment page they must fill in all required fields and **CLICK THE BOX TO ACCEPT THE DOCKDOGS RULES & POLICIES** and click **PROCESS PAYMENT** to process their transaction.

* The Handler will receive an email to the email address on their profile with a receipt showing the event details & Wave(s) they have registered for.

Event Registration Status
-------------------------------

* Events have several status fields that Handlers will encounter when attempting to register for an event.
* Events that say **OPEN** are currently open for online pre-registration.
* Events that say **UPCOMING** are not yet currently open for pre-registration.
* Events that say **CLOSED** are closed, however, onsite registration should be available unless otherwise noted on the event listing.

.. image:: images/upcoming-event.png

.. image:: images/status-open.png

.. image:: images/upcoming-events-full.png



.. target-notes::

.. _`http://dockdogsevents.com`: http://dockdogsevents.com
